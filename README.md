# Node JS Masterclass - Homework Assignment 2

Restful API for pizza delivery app.

## Introduction

This API provides a customer with posibility to sign up, preview a menu, add products to a shopping cart, place an order and pay with a credit card. Payments are handled through Stipe. Once a order is made, the user will receive a confirmation email send through Mailgun.

## Getting Started

### Prerequisites

- [Stripe Account](https://stripe.com/docs/testing#cards)
- [Mailgun Account](https://mailgun.com)
- Node JS

### Installing

- Clone the repo
- Open the lib directoty. Inside you will see and `example.config.js`. Rename the file to `config.js` and updated the fileds where it says `YOUR_...` with your mailgun and stripe api keys.
- Navigate to the repo from the terminal and run `node index.js`. This will start a server that listens for requests on port 3000

## Usage

- Create a user
- Create a token
- Get the menu
- Create a shopping cart
- Create an order

## API Endpoints

### User

#### Create user

Endpoint - `/users`  
Method - `POST`

--- Payload Data ---

Required:

- firstName (string)
- lastName (string)
- email (string)
- password (string)
- streetAddress (string)

#### Get user

Endpoint - `/users`  
Method - `GET`

--- Query String Data ---

Required

- email

--- Headers ---

- token

#### Edit user

Endpoint - `/users`  
Method - `PUT`

--- Payload Data ---

Required:

- email (string)

Optional (at least one must be specified)

- firstName
- lastName
- password
- street address

--- Headers ---

- token

#### Delete user

Endpoint - `/users`  
Method - `DELETE`

--- Payload Data ---

Required

- email (string)

--- Headers ---

- token

### Token

#### Create token

Endpoint - `/tokens`  
Method - `POST`

--- Payload Data ---

Required

- email (string)

#### Get token

Endpoint - `/tokens`  
Method - `GET`

--- Payload Data ---

Required

- email (string)

## Menu

### Get the menu

Endpoint - `/menu`  
Method - `GET`

--- Headers ---

- token

## Shopping cart

### Create shopping cart

Endpoint - `/shopping-carts`  
Method - `POST`

--- Payload Data ---

Required:

- email (string)
- items (array of objects, each consisting of name and quantity( e.g [{name: 'pizza', quantity: 1}]))

--- Headers ---

- token

### Edit shopping cart

Endpoint - `/shopping-carts`  
Method - `PUT`

--- Payload Data ---

Required:

- shoppingCartId (string)
- items (array of objects, each consisting of name and quantity( e.g [{name: 'pizza', quantity: 1}]))

--- Headers ---

- token

### Get shopping cart

Endpoint - `/shopping-carts`  
Method - `GET`

--- Query string data ---

Required:

- shoppingCartId (string)

--- Headers ---

- token

### Delete shopping cart

Endpoint - `/shopping-carts`  
Method - `DELETE`

--- Payload Data ---

Required:

- shoppingCartId (string)

--- Headers ---

- token

## Orders

### Create order

Endpoint - `/orders`  
Method - `POST`

--- Payload Data ---

Required:

- email (string)
- cardNumber (string; length - 16 characters)
- cardExpityDate (string)
- cardHolder (string)
- cardCVVCode (string - 3 characters)
- shoppingCartId (string)

--- Headers ---

- token

Note: Once an order is placed, you will receive a confirmation email

### Get order

Endpoint - `/orders`  
Method - `GET`

--- Query string data ---

Required:

- orderId (string)

--- Headers ---

- token
