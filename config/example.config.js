/**
 * Create and export configuration variables
 */

// Container for all the enviroments
let enviroments = {};


// Staging (default) environment
enviroments.staging = {
    'httpPort': 3000,
    'httpsPort': 3001,
    'envName': 'staging',
    'hashingSecret': 'thisIsASecret',
    'stripe': {
        'apiKey': 'YOUR_API_KEY',
    },
    'mailgun': {
        'accountId': 'YOUR_ACCOUNT_ID',
        'apiKey': 'YOUR_API_KEY'
    }
};

// Production environment
enviroments.production = {
    'httpPort': 5000,
    'httpsPort': 5001,
    'envName': 'production',
    'hashingSecret': 'thisIsAlsoASecret',
    'stripe': {
        'apiKey': 'YOUR_API_KEY',
    },
    'mailgun': {
        'accountId': 'YOUR_ACCOUNT_ID',
        'apiKey': 'YOUR_API_KEY'
    }
};

// Determine which environment was passed as a command-line agrgument
let currnetEnvironemnt = typeof (process.env.NODE_ENV) == 'string' ? process.env.NODE_ENV.toLocaleLowerCase() : '';

// Check that the current environemt is one of the environements above, if not, default to staging
let enviromentToExport = typeof (enviroments[currnetEnvironemnt]) == 'object' ? enviroments[currnetEnvironemnt] : enviroments.staging;

// Export the module
module.exports = enviromentToExport;