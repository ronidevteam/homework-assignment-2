/**
 * Create and export configuration variables
 */

// Container for all the enviroments
let enviroments = {};

// Staging (default) environment
enviroments.staging = {
    'httpPort': 3000,
    'httpsPort': 3001,
    'envName': 'staging',
    'hashingSecret': 'thisIsASecret',
    'stripe': {
        'apiKey': 'sk_test_oUa48d85GYDqVhpgbu2tiao300Do0URKVc',
    },
    'mailgun': {
        'accountId': 'sandbox14e3c2265530435496b2dade6fbf4796',
        'apiKey': 'cdadcded68b906d949a5b0752c2ea3e6-73ae490d-dccdada3'
    }
};

// Production environment
enviroments.production = {
    'httpPort': 5000,
    'httpsPort': 5001,
    'envName': 'production',
    'hashingSecret': 'thisIsAlsoASecret',
    'stripe': {
        'apiKey': 'sk_test_oUa48d85GYDqVhpgbu2tiao300Do0URKVc',
    },
    'mailgun': {
        'accountId': 'sandbox14e3c2265530435496b2dade6fbf4796',
        'apiKey': 'cdadcded68b906d949a5b0752c2ea3e6-73ae490d-dccdada3'
    }
};

// Determine which environment was passed as a command-line agrgument
let currnetEnvironemnt = typeof (process.env.NODE_ENV) == 'string' ? process.env.NODE_ENV.toLocaleLowerCase() : '';

// Check that the current environemt is one of the environements above, if not, default to staging
let enviromentToExport = typeof (enviroments[currnetEnvironemnt]) == 'object' ? enviroments[currnetEnvironemnt] : enviroments.staging;

// Export the module
module.exports = enviromentToExport;