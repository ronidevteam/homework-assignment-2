/**
 * Menu Handlers
 */

// Dependencies
const menu = require('../menu');
const helpers = require('../helpers');
const tokensHandlers = require('./tokens');

// Define menu handlers
const menuHandlers = {};

// Define acceptable methods
menuHandlers.acceptableMethods = ['get'];

// Menu - get
// Require data: email
// Optional data: none
menuHandlers.get = (data, callback) => {
    const token = typeof data.headers.token == 'string' ? data.headers.token : false;
    const email = typeof data.queryStringObject.email == 'string' && data.queryStringObject.email.trim().length > 0 && helpers.validateEmail(data.queryStringObject.email.trim()) ? data.queryStringObject.email.trim() : false;
    if (email) {
        tokensHandlers.verifyToken(token, email, tokenIsValid => {
            if (tokenIsValid) {
                callback(200, menu.getMenu());
            } else {
                callback(403, {
                    Error: 'Missing required token in header, or token is invalid'
                });
            }
        });
    } else {
        callback(400, {
            Error: 'Missing required fields'
        });
    }
};



// Export the module
module.exports = menuHandlers;