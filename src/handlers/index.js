/**
 * Request handlers
 */

// Dependencies
const usersHandlers = require('./users');
const tokensHandlers = require('./tokens');
const menuHandlers = require('./menu');
const shoppingCartsHandlers = require('./shopping-carts');
const ordersHandlers = require('./orders');

// Define handlers
let handlers = {};

/**
 * USERS
 */
handlers.users = (data, callback) => {
    if (usersHandlers.acceptableMethods.indexOf(data.method) > -1) {
        usersHandlers[data.method](data, callback);
    } else {
        callback(405);
    }
};

/**
 * TOKENS
 */
handlers.tokens = (data, callback) => {
    if (tokensHandlers.acceptableMethods.indexOf(data.method) > -1) {
        tokensHandlers[data.method](data, callback);
    } else {
        callback(405);
    }
};


/**
 * MENU
 */
handlers.menu = (data, callback) => {
    if (menuHandlers.acceptableMethods.indexOf(data.method) > -1) {
        menuHandlers[data.method](data, callback);
    } else {
        callback(405);
    }
};

/**
 * SHOPPING CART
 */
handlers.shoppingCarts = (data, callback) => {
    if (shoppingCartsHandlers.acceptableMethods.indexOf(data.method) > -1) {
        shoppingCartsHandlers[data.method](data, callback);
    } else {
        callback(405);
    }
};

/**
 * ORDER
 */
handlers.orders = (data, callback) => {
    if (ordersHandlers.acceptableMethods.indexOf(data.method) > -1) {
        ordersHandlers[data.method](data, callback);
    } else {
        callback(405);
    }
};

/**
 * NOT FOUND
 */
handlers.notFound = (data, callback) => {
    callback(404);
};

// Export the module
module.exports = handlers;