/**
 * Helpers for various tasks
 */

// Dependencies
const crypto = require('crypto');
const config = require('../config/config');
const https = require('https');
const querystring = require('querystring');
const _data = require('./data');

// Container for all helpers
const helpers = {};

// Create SHA256 hash
helpers.hash = str => {
    if (typeof str == 'string' && str.length > 0) {
        return crypto
            .createHmac('sha256', config.hashingSecret)
            .update(str)
            .digest('hex');
    } else {
        return false;
    }
};

// Parse a JSON string to an object in all cases, without throwing
helpers.parseJsonToObject = str => {
    try {
        return JSON.parse(str);
    } catch (e) {
        return {};
    }
};

// Create a string of random alphanumerc charecters of a given length
helpers.createRandomString = strLength => {
    strLength = typeof strLength == 'number' && strLength > 0 ? strLength : false;
    if (strLength) {
        // Define all hte possible charecters taht go into a string
        const possibleCharacters = 'abcdefghijklmnopqrstuvwxyz0123456789';

        // Start the final string
        let str = '';
        for (i = 1; i <= strLength; i++) {
            // Get a random charecter from the possibleCharacters string
            const randomCharacter = possibleCharacters.charAt(
                Math.floor(Math.random() * possibleCharacters.length)
            );
            // Apend this character to the final string
            str += randomCharacter;
        }
        return str;
    } else {
        return false;
    }
};

// Check for valid email
helpers.validateEmail = email => {
    if (typeof (email) == 'string' && email.trim().length > 0) {
        const emailRegexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return emailRegexp.test(email);
    } else {
        return false;
    }
};

// Payment with stripe
helpers.proceedPayment = (amount, callback) => {
    if (typeof (amount) == 'number') {
        // Transform into dolars
        const amountInDolars = amount * 100;
        if (amountInDolars > 500) {
            // Build the post string from an object
            const post_data = querystring.stringify({
                source: 'tok_visa',
                amount: amountInDolars,
                currency: 'usd'
            });
            const post_options = {
                host: 'api.stripe.com',
                protocol: 'https:',
                path: '/v1/charges',
                method: 'POST',
                auth: config.stripe.apiKey
            };

            // Set up the request
            const post_req = https.request(post_options, function (res) {
                let body = '';
                res.on('data', function (chunk) {
                    body += chunk;
                });

                res.on('end', () => {
                    body = JSON.parse(body);
                    // Check if the payment was successful
                    if (!body.error) {
                        callback(false);
                    } else {
                        callback(`Could not make payment. ${body.error}`);
                    }
                });
            });
            // post the data
            post_req.write(post_data);
            post_req.end();
        } else {
            callback('The amount cannot be less than $5 ');
        }
    } else {
        callback('Amount must be a number');
    }
};

helpers.sendEmail = (email, totalAmount, callback) => {
    const emailMessage = `
        Your order was successfully proceeded.
        -----------------------------------------
        Total payed: ${totalAmount} $
    `;

    const requestBody = querystring.stringify({
        from: 'Pizza Place pizza@pizza.com',
        to: email,
        subject: 'Order Confirmation',
        text: emailMessage,
    });

    const options = {
        method: 'POST',
        protocol: 'https:',
        host: 'api.mailgun.net',
        path: `/v3/${config.mailgun.accountId}.mailgun.org/messages`,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': `Basic ${Buffer.from('api:' + config.mailgun.apiKey).toString('base64')}`,
        },
        payload: requestBody
    };

    // Set up the request
    var post_req = https.request(options, function (res) {
        let body = '';
        res.on('data', function (chunk) {
            body += chunk;
        });
        res.on('end', () => {
            body = JSON.parse(body);
            // Check if email was sent successfully
            if (body.id) {
                callback(false);
            } else {
                callback(`Could not send email. ${body.message}`);
            }
        });
    });
    // post the data
    post_req.write(requestBody);
    post_req.end();
};

// Export the module
module.exports = helpers;