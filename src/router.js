/**
 * Define a request router
 */

// Dependencies
const handlers = require('./handlers');

const router = {
    'users': handlers.users,
    'tokens': handlers.tokens,
    'menu': handlers.menu,
    'shopping-carts': handlers.shoppingCarts,
    'orders': handlers.orders
};

module.exports = router;